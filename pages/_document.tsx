import Document, {Html, Head, Main, NextScript} from 'next/document';
import React from 'react';

class MyDocument extends Document {
  render() {
    return (
      <Html style={{backgroundColor: '#161b28'}}>
        <Head>
          <meta charSet="utf-8"/>
          <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Barlow+Condensed:wght@400;700&display=swap"/>
        </Head>

        <body>
        <Main/>
        <NextScript/>
        </body>
      </Html>
    )
  }
}

export default MyDocument;
