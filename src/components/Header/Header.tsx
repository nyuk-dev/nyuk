import React from 'react';

import WidgetHeaderDesktop from './Widgets/WidgetHeaderDesktop';
import WidgetHeaderTouch from './Widgets/WidgetHeaderTouch';

declare interface IHeaderProps {
  keyword?: string;
}

function Header(props: IHeaderProps) {
  return (
    <section className="header">
      <WidgetHeaderDesktop keyword={props.keyword}/>
      <WidgetHeaderTouch keyword={props.keyword}/>
    </section>
  );
}

export default Header;
