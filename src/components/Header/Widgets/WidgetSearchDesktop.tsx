import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSearch} from '@fortawesome/free-solid-svg-icons';
import React, {useRef} from 'react';

declare interface IWidgetSearchProps {
  keyword?: string;
}

function WidgetSearchDesktop(props: IWidgetSearchProps) {
  const inputRef = useRef(null);

  function onSubmit(e: any) {
    e.preventDefault();

    const keyword = inputRef.current.value;
    if (keyword.length < 4) {
      return;
    }

    window.location.href = `/search?q=${keyword}`;
  }

  return (
    <div className="widget-search-desktop">
      <form onSubmit={onSubmit}>
        <div className="search-wrapper">
          <div className="search-input">
            <input className="input" type="text" ref={inputRef} placeholder="Tìm kiếm" defaultValue={props.keyword}/>
          </div>

          <div className="search-button">
            <FontAwesomeIcon icon={faSearch} size='lg'/>
          </div>
        </div>
      </form>
    </div>
  );
}

export default WidgetSearchDesktop;
