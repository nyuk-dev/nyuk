import React from 'react';

import WidgetSearchTouch from './WidgetSearchTouch';

declare interface IWidgetHeaderTouchProps {
  keyword?: string;
}

function WidgetHeaderTouch(props: IWidgetHeaderTouchProps) {
  return (
    <div className="widget-header-touch">
      <div className="header-brand">
        <a href="/">
          <img src={"/static/vikinger.svg"} alt="Logo"/>
        </a>
      </div>

      <WidgetSearchTouch keyword={props.keyword}/>
    </div>
  );
}

export default WidgetHeaderTouch;
