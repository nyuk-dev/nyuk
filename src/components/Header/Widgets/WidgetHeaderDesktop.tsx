import React from 'react';

import WidgetSearchDesktop from './WidgetSearchDesktop';

declare interface IWidgetHeaderDesktopProps {
  keyword?: string;
}

function WidgetHeaderDesktop(props: IWidgetHeaderDesktopProps) {
  return (
    <div className="widget-header-desktop">
      <div className="container">
        <div className="header-brand">
          <a href="/">
            <img src={"/static/vikinger.svg"} alt="Logo"/>
          </a>
        </div>

        <WidgetSearchDesktop keyword={props.keyword}/>
      </div>
    </div>
  );
}

export default WidgetHeaderDesktop;
