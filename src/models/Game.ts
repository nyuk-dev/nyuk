import axios from 'axios';

import {GameModel} from './GameModel';

class Game {
  public static async getBySlug(slug: string): Promise<GameModel> {
    let resp = await axios.get('https://api.igdb.vn/igdb/games?q=where%20slug%3D%22' + slug + '%22;');
    if (resp.data.length != 1) {
      throw new Error('Not Found');
    }

    return resp.data.pop() as GameModel;
  }

  public static async search(keyword: string): Promise<GameModel[]> {
    let resp = await axios.get(`https://api.igdb.vn/igdb/games?q=search "${keyword}"; where release_dates.platform = (6, 48, 49, 130);`);

    return resp.data as GameModel[];
  }
}

export default Game;
