import Error from 'next/error';
import Head from 'next/head';
import React from 'react';

import Header from 'src/components/Header/Header';
import Game from 'src/models/Game';
import {GameModel} from 'src/models/GameModel';

import WidgetItem from './Widgets/WidgetItem/WidgetItem';

declare interface IProps {
  statusCode?: number;
  keyword?: string;
  games: GameModel[];
}

function SearchPage(props: IProps) {
  if (props.statusCode != null) {
    return <Error statusCode={props.statusCode}/>;
  }

  return (
    <section id="Common-SearchPage">
      <Head>
        <title>Search Results</title>
      </Head>

      <Header keyword={props.keyword}/>

      {props.games.length ? <WidgetItems games={props.games}/> : <WidgetNotFound/>}
    </section>
  );
}

export async function getServerSideProps({query}) {
  try {
    const keyword = query.q;
    const games = keyword.length < 4 ? [] : await Game.search(keyword);

    return {
      props: {keyword, games}
    };
  } catch (error) {
    let statusCode = 404;
    if (error.response && error.response.status) {
      statusCode = error.response.status;
    }

    return {
      props: {
        statusCode: statusCode,
      }
    };
  }
}

export default SearchPage;

declare interface IWidgetItemsProps {
  games: GameModel[];
}

function WidgetItems(props: IWidgetItemsProps) {
  return (
    <React.Fragment>
      {props.games.length && props.games.map((game) => {
        return (
          <WidgetItem key={game.slug} game={game}/>
        );
      })}
    </React.Fragment>
  );
}

function WidgetNotFound() {
  return (
    <div className="container mt-5 px-5">
      <p>Không có kết quả cho nội dung bạn cần tìm!?</p>
    </div>
  );
}
