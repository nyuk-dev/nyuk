import moment from 'moment';
import React from 'react';

import {GameModel} from 'src/models/GameModel';

declare interface IWidgetItemIProps {
  game: GameModel;
}

function WidgetItem(props: IWidgetItemIProps) {
  return (
    <React.Fragment>
      <WidgetItemDesktop {...props}/>
      <WidgetItemTouch {...props}/>
    </React.Fragment>
  );
}

export default WidgetItem;

function WidgetItemDesktop(props: IWidgetItemIProps) {
  const {game} = props;

  return (
    <div className="item-desktop container mt-5">
      <div className="columns">
        <div className="column is-narrow">
          <div className="item-cover" style={{backgroundImage: "url('" + game.coverURL + "')"}}/>
        </div>

        <div className="column column-second">
          <p className="item-name">
            <a href={`/games/${game.slug}`}>
              {game.name}
            </a>
          </p>

          <p className="item-section">
            {game.summary}
          </p>

          <p className="item-section">
            <strong>Genres:</strong> {generateGenres(game)}
          </p>
        </div>

        <div className="column column-third is-narrow">
          <p className="item-section">
            <strong>Platforms:</strong> {generatePlatforms(game)}
          </p>

          <p className="item-section">
            <strong>Release Date:</strong> {generateReleaseDate(game)}
          </p>
        </div>
      </div>
    </div>
  );
}

function WidgetItemTouch(props: IWidgetItemIProps) {
  const {game} = props;

  return (
    <div className="item-touch container mt-5 px-5">
      <div className="row-first">
        <div className="item-cover" style={{backgroundImage: "url('" + game.coverURL + "')"}}/>

        <p className="item-name">
          <a href={`/games/${game.slug}`}>
            {game.name}
          </a>
        </p>
      </div>

      <div className="row-second">
        <p className="item-section">
          {game.summary}
        </p>

        <p className="item-section">
          <strong>Genres:</strong> {generateGenres(game)}
        </p>

        <p className="item-section">
          <strong>Platforms:</strong> {generatePlatforms(game)}
        </p>

        <p className="item-section">
          <strong>Release Date:</strong> {generateReleaseDate(game)}
        </p>
      </div>
    </div>
  );
}

function generateGenres(game: GameModel): string {
  let genres = [];
  game.genres.forEach((genre) => {
    genres.push(genre.name);
  })

  return genres.join(', ');
}

function generatePlatforms(game: GameModel): string {
  let platforms = [];
  game.platforms.forEach((platform) => {
    platforms.push(platform.abbreviation);
  })

  return platforms.join(', ');
}

function generateReleaseDate(game: GameModel): string {
  return moment.utc(game.firstReleaseDate, 'X').format('MMM Do, YYYY');
}
