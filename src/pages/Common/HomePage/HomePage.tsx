import React from 'react';

import Header from 'src/components/Header/Header';

function HomePage() {
  return (
    <section id="Common-HomePage">
      <Header/>
    </section>
  );
}

export default HomePage;
