import Error from 'next/error';
import Head from 'next/head';
import React from 'react';

import Header from 'src/components/Header/Header';
import Game from 'src/models/Game';
import {GameModel} from 'src/models/GameModel';

import WidgetHeading from './Widgets/WidgetHeading/WidgetHeading';

declare interface IProps {
  statusCode?: number;
  game: GameModel;
}

function SinglePage(props: IProps) {
  if (props.statusCode != null) {
    return <Error statusCode={props.statusCode}/>;
  }

  return (
    <section id="Games-SinglePage">
      <Head>
        <title>{props.game.name}</title>
      </Head>

      <Header/>

      <WidgetHeading game={props.game}/>
    </section>
  );
}

export async function getServerSideProps({query}) {
  try {
    const game = await Game.getBySlug(query.slug);

    return {
      props: {game}
    };
  } catch (error) {
    let statusCode = 404;
    if (error.response && error.response.status) {
      statusCode = error.response.status;
    }

    return {
      props: {
        statusCode: statusCode,
      }
    };
  }
}

export default SinglePage;
