import moment from 'moment';
import React from 'react';

import {GameModel} from 'src/models/GameModel';

declare interface IWidgetHeadingProps {
  game: GameModel;
}

function WidgetHeading(props: IWidgetHeadingProps) {
  return (
    <React.Fragment>
      <WidgetHeadingDesktop {...props}/>
      <WidgetHeadingTouch {...props}/>
    </React.Fragment>
  );
}

export default WidgetHeading;

function WidgetHeadingDesktop(props: IWidgetHeadingProps) {
  const {game} = props;

  return (
    <div className="widget-heading">
      <div className="container px-5">
        <div className="columns">
          <div className="column is-narrow">
            <div className="heading-cover" style={{backgroundImage: "url('" + game.coverURL + "')"}}/>
          </div>

          <div className="heading-second column">
            <p className="item-title">
              {game.name}
            </p>

            <p className="item-section">
              {game.summary}
            </p>

            <p className="item-section">
              <strong>Genres:</strong> {generateGenres(game)}
            </p>
          </div>

          <div className="heading-third column is-narrow">
            <p className="item-section">
              <strong>Platforms:</strong> {generatePlatforms(game)}
            </p>

            <p className="item-section">
              <strong>Release Date:</strong> {generateReleaseDate(game)}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

function WidgetHeadingTouch(props: IWidgetHeadingProps) {
  const {game} = props;

  return (
    <div className="widget-heading-touch">
      <div className="container px-5">
        <div className="heading-first">
          <div className="item-cover" style={{backgroundImage: "url('" + game.coverURL + "')"}}/>

          <p className="item-name">
            {game.name}
          </p>
        </div>

        <div className="heading-second">
          <p className="item-section">
            {game.summary}
          </p>

          <p className="item-section">
            <strong>Genres:</strong> {generateGenres(game)}
          </p>

          <p className="item-section">
            <strong>Platforms:</strong> {generatePlatforms(game)}
          </p>

          <p className="item-section">
            <strong>Release Date:</strong> {generateReleaseDate(game)}
          </p>
        </div>
      </div>
    </div>
  );
}

function generateGenres(game: GameModel): string {
  let genres = [];
  game.genres.forEach((genre) => {
    genres.push(genre.name);
  })

  return genres.join(', ');
}

function generatePlatforms(game: GameModel): string {
  let platforms = [];
  game.platforms.forEach((platform) => {
    platforms.push(platform.abbreviation);
  })

  return platforms.join(', ');
}

function generateReleaseDate(game: GameModel): string {
  return moment.utc(game.firstReleaseDate, 'X').format('MMM Do, YYYY');
}
